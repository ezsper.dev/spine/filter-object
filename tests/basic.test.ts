/// <reference types="jest" />
import { filterObject } from '..';

describe('Basic', () => {
  const obj = {
    foo: 'bar',
    htmlPage: {
      title: 'Hello World',
      metas: [
        { name: 'description', content: 'My Hello World Page' },
      ],
    },
    environment: {
      NODE_ENV: 'production',
      FOO: 'bar',
    },
  };

  it('Results', async () => {
    expect(JSON.stringify(filterObject(obj, {}))).toBe('{}');
    expect(JSON.stringify(filterObject(obj, { foo: true }))).toBe('{"foo":"bar"}');
    expect(JSON.stringify(filterObject(obj, { htmlPage: { title: true } }))).toBe('{"htmlPage":{"title":"Hello World"}}');
    expect(JSON.stringify(filterObject(obj, { htmlPage: { metas: { name: true } } }))).toBe('{"htmlPage":{"metas":[{"name":"description"}]}}');
    expect(JSON.stringify(filterObject(obj, { environment: { 'NODE_ENV': true } }))).toBe('{"environment":{"NODE_ENV":"production"}}');
    expect(JSON.stringify(filterObject(obj, { environment: { '*': true } }))).toBe('{"environment":{"NODE_ENV":"production","FOO":"bar"}}');
  });

});