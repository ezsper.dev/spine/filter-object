/**
 * Utility type to remove nil values
 */
type Unaoptional<T> = Exclude<T, undefined | null | void>;

/**
 * Utility type to unpack type from array
 */
type Unpacked<T> = T extends (infer U)[] ? U : T;

/**
 * Utility type to repack item if it's an array
 */
export type PackHelper<T, U> = T extends any[]
  ? U[]
  : U;

/**
 * Utility type to restore optional values
 */
export type OptionalHelper<T, U> = T extends undefined | null | void
  ? Exclude<T, Exclude<T, undefined | null | void>> | U
  : U;

export type FilterObjectRule = true | false | { [key: string]: FilterObjectRule };

export type FilterObjectMatchRule<T> =
  T extends Unpacked<Unaoptional<infer U>>
    ? (U extends Function | boolean | string | number | null | undefined
      ? true | false
      : (
        { '*'?: FilterObjectMatchRule<T[keyof T]> }
        & { [K in keyof T]?: FilterObjectMatchRule<T[K]> }
        ) | true | false
      )
    : never;

export type FilterObjectNest<T, I> =
  OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U
    ? (
      I extends FilterObjectMatchRule<U>
        ? (
          I extends true | false
            ? (I extends true ? U : never)
            : (U extends Function | boolean | string | number | null | undefined
              ? U
              : (
                (I extends { '*': FilterObjectMatchRule<U[keyof U]> }
                  ? { [K in keyof U]: (K extends keyof U ? FilterObjectNest2<U[K], I['*']> : never) }
                  : {}
                ) & ({ [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest2<U[K], I[K]> : never) })
              )
            )
          )
        : never
      )
    : never>>;

export type FilterObjectNest2<T, I> =
  OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U
    ? (
      I extends FilterObjectMatchRule<U>
        ? (
          I extends true | false
            ? (I extends true ? U : never)
            : (U extends Function | boolean | string | number | null | undefined
            ? U
            : (
              (I extends { '*': FilterObjectMatchRule<U[keyof U]> }
                ? { [K in keyof U]: (K extends keyof U ? FilterObjectNest3<U[K], I['*']> : never) }
                : {}
                ) & ({ [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest3<U[K], I[K]> : never) })
              )
              )
          )
        : never
      )
    : never>>;

export type FilterObjectNest3<T, I> =
  OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U
    ? (
      I extends FilterObjectMatchRule<U>
        ? (
          I extends true | false
            ? (I extends true ? U : never)
            : (U extends Function | boolean | string | number | null | undefined
            ? U
            : (
              (I extends { '*': FilterObjectMatchRule<U[keyof U]> }
                ? { [K in keyof U]: (K extends keyof U ? FilterObjectNest4<U[K], I['*']> : never) }
                : {}
                ) & ({ [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest4<U[K], I[K]> : never) })
              )
              )
          )
        : never
      )
    : never>>;

export type FilterObjectNest4<T, I> =
  OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U
    ? (
      I extends FilterObjectMatchRule<U>
        ? (
          I extends true | false
            ? (I extends true ? U : never)
            : (U extends Function | boolean | string | number | null | undefined
            ? U
            : (
              (I extends { '*': FilterObjectMatchRule<U[keyof U]> }
                ? { [K in keyof U]: (K extends keyof U ? FilterObjectNest5<U[K], I['*']> : never) }
                : {}
                ) & ({ [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest5<U[K], I[K]> : never) })
              )
              )
          )
        : never
      )
    : never>>;

export type FilterObjectNest5<T, I> =
  OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U
    ? (
      I extends FilterObjectMatchRule<U>
        ? (
          I extends true | false
            ? (I extends true ? U : never)
            : (U extends Function | boolean | string | number | null | undefined
            ? U
            : (
              (I extends { '*': FilterObjectMatchRule<U[keyof U]> }
                ? { [K in keyof U]: (K extends keyof U ? FilterObjectNest<U[K], I['*']> : never) }
                : {}
                ) & ({ [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest<U[K], I[K]> : never) })
              )
              )
          )
        : never
      )
    : never>>;



export type FilterObject<T, I extends FilterObjectMatchRule<T>> = FilterObjectNest<T, I>;

function cloneObject(obj: any): any {
  if (typeof obj !== 'object' || obj == null) {
    return obj;
  }
  if (Array.isArray(obj)) {
    return obj.map(cloneObject);
  }
  const newObj: any = {};
  for (const key of Object.keys(obj)) {
    newObj[key] = cloneObject(obj[key]);
  }
  return newObj;
}


export function filterObject<T, FilterInput extends FilterObjectRule & FilterObjectMatchRule<T>>(value: T, filter: FilterInput): FilterObject<T, FilterInput> {
  if (typeof filter === 'boolean' || (typeof value !== 'object' || value == null)) {
    if (filter) {
      return cloneObject(value);
    }
    return <never>void 0;
  }
  if (value == null) {
    return <never>void 0;
  }
  if (Array.isArray(value)) {
    return <any>value.map(item => filterObject(item, <any>filter));
  }
  const filtered: any = {};
  if ((<any>filter)['*'] != null) {
    for (const key of Object.keys(value)) {
      filtered[key] = filterObject((<any>value)[key], (<any>filter)['*']);
    }
  }
  for (const key of Object.keys(filter)) {
    if (key === '*') {
      continue;
    }
    filtered[key] = filterObject((<any>value)[key], (<any>filter)[key]);
  }
  return filtered;
}

