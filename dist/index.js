"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterObject = filterObject;

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.object.keys");

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

/**
 * Utility type to remove nil values
 */

/**
 * Utility type to unpack type from array
 */

/**
 * Utility type to repack item if it's an array
 */

/**
 * Utility type to restore optional values
 */
function cloneObject(obj) {
  if ((0, _typeof2.default)(obj) !== 'object' || obj == null) {
    return obj;
  }

  if (Array.isArray(obj)) {
    return obj.map(cloneObject);
  }

  var newObj = {};

  var _arr = Object.keys(obj);

  for (var _i = 0; _i < _arr.length; _i++) {
    var _key = _arr[_i];
    newObj[_key] = cloneObject(obj[_key]);
  }

  return newObj;
}

function filterObject(value, filter) {
  if (typeof filter === 'boolean' || (0, _typeof2.default)(value) !== 'object' || value == null) {
    if (filter) {
      return cloneObject(value);
    }

    return void 0;
  }

  if (value == null) {
    return void 0;
  }

  if (Array.isArray(value)) {
    return value.map(function (item) {
      return filterObject(item, filter);
    });
  }

  var filtered = {};

  if (filter['*'] != null) {
    var _arr2 = Object.keys(value);

    for (var _i2 = 0; _i2 < _arr2.length; _i2++) {
      var _key2 = _arr2[_i2];
      filtered[_key2] = filterObject(value[_key2], filter['*']);
    }
  }

  var _arr3 = Object.keys(filter);

  for (var _i3 = 0; _i3 < _arr3.length; _i3++) {
    var _key3 = _arr3[_i3];

    if (_key3 === '*') {
      continue;
    }

    filtered[_key3] = filterObject(value[_key3], filter[_key3]);
  }

  return filtered;
}
//# sourceMappingURL=index.js.map