/**
 * Utility type to remove nil values
 */
declare type Unaoptional<T> = Exclude<T, undefined | null | void>;
/**
 * Utility type to unpack type from array
 */
declare type Unpacked<T> = T extends (infer U)[] ? U : T;
/**
 * Utility type to repack item if it's an array
 */
export declare type PackHelper<T, U> = T extends any[] ? U[] : U;
/**
 * Utility type to restore optional values
 */
export declare type OptionalHelper<T, U> = T extends undefined | null | void ? Exclude<T, Exclude<T, undefined | null | void>> | U : U;
export declare type FilterObjectRule = true | false | {
    [key: string]: FilterObjectRule;
};
export declare type FilterObjectMatchRule<T> = T extends Unpacked<Unaoptional<infer U>> ? (U extends Function | boolean | string | number | null | undefined ? true | false : ({
    '*'?: FilterObjectMatchRule<T[keyof T]>;
} & {
    [K in keyof T]?: FilterObjectMatchRule<T[K]>;
}) | true | false) : never;
export declare type FilterObjectNest<T, I> = OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U ? (I extends FilterObjectMatchRule<U> ? (I extends true | false ? (I extends true ? U : never) : (U extends Function | boolean | string | number | null | undefined ? U : ((I extends {
    '*': FilterObjectMatchRule<U[keyof U]>;
} ? {
    [K in keyof U]: (K extends keyof U ? FilterObjectNest2<U[K], I['*']> : never);
} : {}) & ({
    [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest2<U[K], I[K]> : never);
})))) : never) : never>>;
export declare type FilterObjectNest2<T, I> = OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U ? (I extends FilterObjectMatchRule<U> ? (I extends true | false ? (I extends true ? U : never) : (U extends Function | boolean | string | number | null | undefined ? U : ((I extends {
    '*': FilterObjectMatchRule<U[keyof U]>;
} ? {
    [K in keyof U]: (K extends keyof U ? FilterObjectNest3<U[K], I['*']> : never);
} : {}) & ({
    [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest3<U[K], I[K]> : never);
})))) : never) : never>>;
export declare type FilterObjectNest3<T, I> = OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U ? (I extends FilterObjectMatchRule<U> ? (I extends true | false ? (I extends true ? U : never) : (U extends Function | boolean | string | number | null | undefined ? U : ((I extends {
    '*': FilterObjectMatchRule<U[keyof U]>;
} ? {
    [K in keyof U]: (K extends keyof U ? FilterObjectNest4<U[K], I['*']> : never);
} : {}) & ({
    [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest4<U[K], I[K]> : never);
})))) : never) : never>>;
export declare type FilterObjectNest4<T, I> = OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U ? (I extends FilterObjectMatchRule<U> ? (I extends true | false ? (I extends true ? U : never) : (U extends Function | boolean | string | number | null | undefined ? U : ((I extends {
    '*': FilterObjectMatchRule<U[keyof U]>;
} ? {
    [K in keyof U]: (K extends keyof U ? FilterObjectNest5<U[K], I['*']> : never);
} : {}) & ({
    [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest5<U[K], I[K]> : never);
})))) : never) : never>>;
export declare type FilterObjectNest5<T, I> = OptionalHelper<T, PackHelper<T, Unaoptional<Unpacked<T>> extends infer U ? (I extends FilterObjectMatchRule<U> ? (I extends true | false ? (I extends true ? U : never) : (U extends Function | boolean | string | number | null | undefined ? U : ((I extends {
    '*': FilterObjectMatchRule<U[keyof U]>;
} ? {
    [K in keyof U]: (K extends keyof U ? FilterObjectNest<U[K], I['*']> : never);
} : {}) & ({
    [K in Exclude<keyof I, '*'>]: (K extends keyof U ? FilterObjectNest<U[K], I[K]> : never);
})))) : never) : never>>;
export declare type FilterObject<T, I extends FilterObjectMatchRule<T>> = FilterObjectNest<T, I>;
export declare function filterObject<T, FilterInput extends FilterObjectRule & FilterObjectMatchRule<T>>(value: T, filter: FilterInput): FilterObject<T, FilterInput>;
export {};
