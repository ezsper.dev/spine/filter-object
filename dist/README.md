# Spine Filter Object

An utility to filter object by nested rules with extracted types.

## Key aspects

* Smart typings, it filters in typing level (no `any`)
* Readable filter statements

### Rule

```
type FilterObjectRule = true | false | { [key: string]: Rule }
```


## Usage

```
const obj = {
  foo: 'bar',
  htmlPage: {
    title: 'Hello World',
    metas: [
      { name: 'description', content: 'My Hello World Page' },
    ],
  },
  environment: {
    NODE_ENV: 'production',
    FOO: 'bar',
  }
};

console.log(JSON.stringify(filterObject(obj, { foo: true, htmlPage: { title: true } })));
// {"foo":"bar","htmlPage":{"title":"Hello World"}}

console.log(JSON.stringify(filterObject(obj, { htmlPage: { metas: { name: true } } })));
// {"htmlPage":{"metas":[{"name":"description"}]}}

console.log(JSON.stringify(filterObject(obj, { environemnt: { NODE_ENV: true } })));
// {"environment":{"NODE_ENV":"production"}}

console.log(JSON.stringify(filterObject(obj, { environemnt: true })));
// {"environment":{"NODE_ENV":"production","FOO":"bar"}}

console.log(JSON.stringify(filterObject(obj, { environemnt: { '*': true } })));
// {"environment":{"NODE_ENV":"production","FOO":"bar"}}

console.log(JSON.stringify(filterObject(obj, true)));
// {"foo":"bar","htmlPage":{"title":"Hello World","metas":[{"name":"description","content":"My Hello World Page"}]}}

console.log(JSON.stringify(filterObject(obj, {})));
// {}
```
